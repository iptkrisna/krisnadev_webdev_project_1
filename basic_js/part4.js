true
false

// Comparison Operators
3 > 2;
2 > 3;

1 < 2;

2 >= 2;

1 <= 3;

2 == 2;
"username" == "username";

2 != 3;

"2" == 2;

5 === 5;
5 === "5";

5 !== "5";
5 !== 5;

true == 1;
true === 1;

false == 0;
false === 0;

null == undefined;
NaN == NaN;

// Logical Operators
1 === 1 && 2 === 2;

1 === 2 || 1 === 1;

!(1 === 1);

!!(1 === 1);