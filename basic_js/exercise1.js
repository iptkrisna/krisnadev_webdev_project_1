var x = 1
var y = 2

"2" == y && x === 1;

x >= 0 || y === "2";

!(x !== 1) && y === (1+1);

y !== "2" && x < 10;

y !== x || y == "2" || x === 3;

// Control Flow
var hot = false 
var temp = 60

if (temp > 80) {
    hot = true
}
console.log(hot)

var temp = 100

if (temp > 80) {
    hot = true
}
console.log(hot)

temp = 30

if (temp > 90) {
    console.log("Hot outside!")
} else {
    console.log("Its not too hot today!")
}

temp = 75

if (temp > 80) {
    console.log("Hot outside!")
} else if (temp >= 50) {
    console.log("Nice outside!")
} else if (temp >= 32) {
    console.log("Its cooler outside!")
} else {
    console.log("Its really cool outside!")
}

var ham = 10
var cheese = 10

var report = 'blank'

if (ham >= 10 && cheese >= 10) {
    report = "Strong sales of both items"
} else if (ham === 0 && cheese === 0) {
    report = "Nothing sold!"
} else {
    report = "We had some sales"
}
console.log(report)

// While Loops
var x = 0

while (x < 5) {
    console.log("x is currently: " + x);
    console.log("x is still less than 5, adding 1 to x");

    x += 1;
}

var x = 0 

while (x < 5) {
    console.log("x is currently; " + x)

    if (x === 3) {
        console.log("x is equal to 3, BREAK")
        break;
    }

    console.log("x is still less than 5, adding 1 to x");

    x += 1;
}