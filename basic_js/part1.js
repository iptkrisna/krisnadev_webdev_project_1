// CONSOLE
alert("Hello World.")

10
10.5
-10

"Hello World."
"10"

true
false

undefined
null

// NUMBERS
2 + 2
5 - 1
3 * 2
10 / 2
2 / 5
2 ** 4
15 % 14
6 % 2
6 % 4

// STRINGS
"django is cool"
'Django is cool'

"Django" + " is cool"

"Django".length
"four four".length

"hello \n start a new line"
"hello \t give me a tab"
"hello \"quotes\" "

"hello"[0]
"hello"[4]

// VARIABLES
var bankAccount = 100;
var deposit = 50;
var total = bankAccount + deposit;

var greeting = "Welcome back: ";
var name = "Jose";
alert(greeting + name)

var myVariable

var bonus = null

// Basic
alert("hello world")

console.log("Hello World")

var age = prompt("How old are you?")
