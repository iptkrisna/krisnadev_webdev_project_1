for (i = 0; i < 5; i += 1) {
    console.log("Number is " + i)
}

for (i = 0; i < 5; i++) {
    console.log("Number is " + i)
}

var word = "ABDEFGHIJK"
for (i = 0; i < word.length; i++) {
    console.log(word[i])
}

var word = "ABABABABABABABA"
for (i = 0; i < word.length; i+=2) {
    console.log(word[i])
}

while (i <= 100) {
    if (i % 2 == 0) {
        console.log(i);
    }
    i += 1;
}